const fs = require('fs').promises;
const faker = require('faker');
let j = 0;
const filePath = 'JSON_part1';

async function createJsondataCallback(callback) {
    const obj = {
        name: faker.name.findName(),
        age: faker.datatype.number({ min: 18, max: 99 }),
    };
    await callback(obj, j, deleteFile);
    j++;
}

async function createFiles(obj, j, deleteFileCallback) {
    const content = JSON.stringify(obj, null, 2);
    const filename = `${filePath}/file${j + 1}.json`;

    try {
        await fs.writeFile(filename, content);
        console.log(`${filename} file created`);

        if (j === 2) {
            j=3;
            deleteFileCallback(filename);
        }
    } catch (error) {
        console.log(error);
    }
}

async function deleteFile(filename) {
    try {
        await fs.unlink(filename);
        console.log(`${filename} file deleted`);
        j-=1;

        if (j > 0) {
            const prevFilename = `${filePath}/file${j}.json`;
            await deleteFile(prevFilename);
            
        } else {
            console.log('Done');
        }
    } catch (error) {
        console.log(error);
    }
}

async function main() {
    try {
        await fs.mkdir(filePath);
        for (let i = 0; i < 3; i++) {
            await createJsondataCallback(createFiles);
        }
    } catch (error) {
        console.error(error);
    }
}

main();
