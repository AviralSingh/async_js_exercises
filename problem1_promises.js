const fs = require('fs').promises;
const faker = require('faker');

filePath = 'JSON_part1';
fs.mkdir(filePath);

function createJsondataCallback(callback, filename) {
    const obj = {
        name: faker.name.findName(),
        age: faker.datatype.number({ min: 18, max: 99 }),
    };
    return callback(obj, filename);
}

function createFiles(obj, filename) {

    let content = JSON.stringify(obj, null, 2);
    return new Promise((resolve, reject) => {
        fs.writeFile(filename, content, (error, data) => {
            if (error) {
                reject();
            }
            else {
                resolve(data);
            }
        })
    });
}

function deleteFile(filename) {
    return new Promise((resolve, reject) => {
        fs.unlink(filename, (err) => {
            if (err) {
                reject(err);
            }
            else {
                resolve('Files deleted concurrently with promises.');
            }
        });
    });
}

createJsondataCallback(createFiles, `${filePath}/file1.json`).then(createJsondataCallback(createFiles, `${filePath}/file2.json`)).then(createJsondataCallback(createFiles, `${filePath}/file3.json`)).then(Promise.all([
    deleteFile('JSON_part1/file1.json'),
    deleteFile('JSON_part1/file2.json'),
    deleteFile('JSON_part1/file3.json'),
])).then((message) => { console.log(message); });