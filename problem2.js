const fs = require('fs').promises;
const path = require('path');

async function read_file(filePath) {
    return await fs.readFile(filePath, 'utf8');
}

async function uppercaseContent(fileContent) {
    await fs.writeFile('uppercase.txt', fileContent.toUpperCase());
    return 'uppercase.txt';
}

async function lowercaseContent(uppercaseFile) {
    let content = await fs.readFile(uppercaseFile, 'utf8');
    content = content.toLowerCase();
    await fs.writeFile('lowercase.txt', content);
    let filenames = [];
    const sentences = content.split('.');
    for (let i = 0; i < sentences.length; i++) {
        await fs.writeFile(`sentence${i + 1}.txt`, sentences[i]);
        filenames.push(`sentence${i + 1}.txt`);
    }
    const sortedFilename = 'sorted.txt';
    await fs.writeFile(sortedFilename, ''); 
    filenames.push(sortedFilename);
    await fs.writeFile('filenames.txt', filenames.join('\n'));
    return 'filenames.txt';
}

async function sortedContent(name) {
    let filenames = await fs.readFile(name, 'utf8');
    let unsorted_content = [];
    filenames = filenames.split('\n');
    for (let i of filenames) {
        let data = await fs.readFile(i, 'utf8');
        data = data.trim().split(' ');
        unsorted_content = unsorted_content.concat(data);
    }
    let sorted_content = unsorted_content.sort();

    const sortedContentString = sorted_content.join(' ');
    await fs.writeFile('sorted.txt', sortedContentString);
}

async function deleteFilesConcurrently(filenameList) {
    const filenames = filenameList.split('\n');
    const deletePromises = filenames.map(async (filename) => {
        const filePath = path.join(__dirname, filename);
        try {
            await fs.unlink(filePath);
            console.log(`Deleted file: ${filename}`);
        } catch (error) {
            console.error(`Error deleting file ${filename}: ${error.message}`);
        }
    });

    await Promise.all(deletePromises);
    console.log('All files deleted.');
}

async function main() {
    const fileContent = await read_file('lipsum.txt');
    const uppercaseFile = await uppercaseContent(fileContent);
    const names = await lowercaseContent(uppercaseFile);
    await sortedContent(names);

    const filenamesList = await read_file('filenames.txt');
    await deleteFilesConcurrently(filenamesList);
}

main();
