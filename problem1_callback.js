const fs = require('fs').promises;
const faker = require('faker');
let j = 0;
const filePath = 'JSON_part1';

fs.mkdir(filePath)
  .then(() => {
    for (let i = 0; i < 3; i++) {
      createJsondataCallback(createFiles);
    }
  })
  .catch((error) => console.error(error));

function createJsondataCallback(callback) {
  const obj = {
    name: faker.name.findName(),
    age: faker.datatype.number({ min: 18, max: 99 }),
  };
  callback(obj, j, deleteFile);
  j++;
}

function createFiles(obj, j, deleteFileCallback) {
  const content = JSON.stringify(obj, null, 2);
  const filename = `${filePath}/file${j + 1}.json`;

  fs.writeFile(filename, content)
    .then(() => {
      console.log(`${filename} file created`);
      if (j === 2) {
        deleteFileCallback(filename);
      }
    })
    .catch((error) => console.log(error));
}

function deleteFile(filename) {
  fs.unlink(filename)
    .then(() => {
      console.log(`${filename} file deleted`);
      j--;

      if (j > 0) {
        const prevFilename = `${filePath}/file${j}.json`;
        deleteFile(prevFilename);
      } else {
        console.log('Done');
      }
    })
    .catch((error) => console.log(error));
}
